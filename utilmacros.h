#ifndef CUTILITIES_UTILMACROS_H
#define CUTILITIES_UTILMACROS_H
#pragma once

#ifdef __cplusplus
#include <string>
#endif

#define CUTILITIES_EXPORT_C_BEGIN \
    #ifdef __cplusplus \
    extern "C" { \
    #endif

#define CUTILITIES_EXPORT_C_END \
    #ifdef __cplusplus \
    };\
    #endif

#define STR(x) #x
#ifdef __cplusplus
    #define AUTOSTR(s) std::string s = STR(x)
#else
    #define AUTOSTR(s) char* s = STR(x)
#endif

#endif // CUTILITIES_UTILMACROS_H
